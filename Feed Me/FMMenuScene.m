//
//  FMMenuScene.m
//  Feed Me
//
//  Created by Ryan Salton on 05/01/2015.
//  Copyright (c) 2015 Ryan Salton. All rights reserved.
//

#import "FMMenuScene.h"

@interface FMMenuScene ()

@property (nonatomic, strong) SKSpriteNode *feedLabel;
@property (nonatomic, strong) SKSpriteNode *meLabel;
@property (nonatomic, strong) SKSpriteNode *playButton;

@end

@implementation FMMenuScene

-(id)initWithSize:(CGSize)size {
    if (self = [super initWithSize:size]) {
        /* Setup your scene here */
        
        self.backgroundColor = [UIColor colorWithRed:0.0/255.0 green:217.0/255.0 blue:217.0/255.0 alpha:1.0];
        
        // Add BG elements
        
        SKSpriteNode *deskBG = [SKSpriteNode spriteNodeWithImageNamed:@"bg-desk"];
        deskBG.position = CGPointMake(self.size.width * 0.5, deskBG.size.height * 0.5);
        [self addChild:deskBG];
        
        SKSpriteNode *wiresBG = [SKSpriteNode spriteNodeWithImageNamed:@"bg-wires"];
        wiresBG.position = CGPointMake(self.size.width * 0.5, self.size.height - 15);
        [self addChild:wiresBG];
        
        self.feedLabel = [SKSpriteNode spriteNodeWithImageNamed:@"label-feed"];
        self.feedLabel.position = CGPointMake(self.size.width * 0.5, self.size.height - (self.feedLabel.size.height * 0.5) - 90);
        [self addChild:self.feedLabel];
        
        self.meLabel = [SKSpriteNode spriteNodeWithImageNamed:@"label-me"];
        self.meLabel.position = CGPointMake(self.size.width * 0.5, (self.feedLabel.position.y - (self.feedLabel.size.height * 0.5)) - (self.meLabel.size.height * 0.5) - 10);
        [self addChild:self.meLabel];
        
        // Add main interface
        
        self.playButton = [SKSpriteNode spriteNodeWithImageNamed:@"icon-play"];
        self.playButton.position = CGPointMake(self.size.width * 0.5, self.meLabel.position.y - (self.meLabel.size.height * 0.5) - (self.playButton.size.height * 0.5) - 40);
        self.playButton.name = @"playButton";
        [self addChild:self.playButton];
        
    }
    return self;
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    /* Called when a touch begins */
    
    for (UITouch *touch in touches) {
        CGPoint location = [touch locationInNode:self];
        SKNode *nodeAtPoint = [self nodeAtPoint:location];
        
        if ([nodeAtPoint.name isEqualToString:@"playButton"])
        {
            
        }
        
    }
}

-(void)update:(CFTimeInterval)currentTime {
    /* Called before each frame is rendered */
}

@end
